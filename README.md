# transfer_learning_CNN

画像処理、CNN系の転移学習の実験リポジトリ。

##  やりたいこと。

- web上でフリーで取得できる画像の情報をここでまとめたい。

- 画像処理の転移学習実験を簡単に実施したい。

- 複数モデルの性能比較など簡単に行いたい。

- 前処理を可視化したい。

- 鳥とか分類できるようになりたい。

- 深層学習以外の画像処理やOpencvもさわりたい。

## 要件	

※まずは、PyTorchでつくる。

### 画像の取得(いろんな画像の取得方法)

- mnisit
- ファッションmnisit
- その他

### 前処理
    
いろんな処理を関数化、ライブラリをつくる。

データオーギュメンテーション

### モデル

いろんなモデルをファインチューニング/転移学習

活性化関数を変更

### 後処理

判断根拠の可視化
- GradCan
- LRP(Layer-wise )



##

